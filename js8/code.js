/*ДЗ : Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
 При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript
 При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.
*/

let startBtn = document.getElementById('btn_start');
let diametr;
startBtn.onclick = (e) => {
	let input = document.createElement("input"); 
    input.placeholder = "Розмір кола:";
	let drowBtn = document.createElement("button"); 
	document.body.appendChild(drowBtn);
	drowBtn.innerText = "Намалювати";	
	drowBtn.insertAdjacentHTML("beforebegin", "<p>Введіть діаметр:</p> ");
	drowBtn.insertAdjacentHTML("afterend", "<br>"); 
	document.body.appendChild(input);
	let box = document.createElement("div");
	box.className = "box";
	document.body.appendChild(box);
    drowBtn.onclick = (e) => {
        for (var i = 0; i < 10; i++) {
for (var j = 0; j < 10; j++) {
                diametr = +input.value;
                let div = document.createElement("div");
                div.className = "circle";
                div.style.width = diametr+"px";
                div.style.height = diametr+"px";
                box.style.width = 12*diametr+"px";
                box.style.height = 12*diametr+"px";
                box.appendChild(div);
            }
          }
    let сircles = document.querySelectorAll(".circle");
    сircles.forEach(function (сirc) {
        сirc.onclick = (e) => {
            e.target.remove();
            сircles = document.querySelectorAll(".circle");
            box.style.height = (сircles.length/10)*(diametr) +"px";
        }
    })        
}
}


/* Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати у імені, що викликає, і прізвище.
Використовуючи дані, введені користувачем, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені користувача, з'єднану з прізвищем користувача, 
все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити користувача за допомогою функції createNewUser(). Викликати користувача функцію getLogin(). 
Вивести у консоль результат виконання функції.
2) Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
Візьміть виконане завдання вище (створена вами функція createNewUser()) та доповніть її наступним функціоналом:
При виклику функція повинна запитати у дату народження, що викликає, (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі,
поєднану з прізвищем (у нижньому регістрі) та роком народження. (Наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
3) Реалізувати функцію фільтру масиву за вказаним типом даних.
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, 
тип яких був переданий другим аргументом. 
Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].*/
//1
class createNewUser{
	constructor(firstName){
		this.firstName = firstName;
		this.lastName = prompt(`Добрий день ${this.firstName},  Прізвище`);
		this.birthday = prompt(`Дата народження у форматі dd.mm.yyyy`);
	}
	getLogin(){
		var firstLetter = this.firstName.charAt(0);
		let login = firstLetter + this.lastName;
		return login.toLowerCase();
	}

    //2
	getAge(){
		let birthYear = this.birthday.slice(6);
		parseInt(birthYear);
		let age = 2023 - birthYear;
		return age;
	}
	getPassword(login,age){
		let password = login.slice(0,1).toUpperCase() + login.slice(1) + this.birthday.slice(6);
		return password;
	}
}
let Name = "Name";
const User = new createNewUser(Name);
console.log(User);
console.log(`Login: ${User.getLogin()}`);
console.log(`Age: ${User.getAge()}`);
console.log(`Password: ${User.getPassword(User.getLogin(),User.getAge())}`);
let array = ['hello', 'world', 23, '23', null];
let type = 'string';
let tmpArr = [];
//3
function filterBy(array, type){
	for(let i = 0; i < array.length; i++)
		if(typeof(array[i]) === typeof(type)) { array.splice(i,1); i--; }
	return array;
}
console.log(filterBy(array, type));